![JAboutDialog](icon64.png)

# JAboutDialog

Javaで作られたアプリの"Aboutダイアログ"の生成を容易にします。`javax.swing.JDIalog`を継承しています。

## バイナリのダウンロード

サンプルアプリ(実行可能Jarファイル)は以下からダウンロードをお願いします。

https://kasu-kasu.ga/post/jaboutdialog-relased-as-cc0/

# 必要なもの

- Java8 またはそれ以降
- Mavenを使用可能なPC

# 使用方法

## Mavenでのビルド

JAboutDialogはライブラリとして必要なファイルのみをビルドする`pom.xml`と使用イメージを把握するためのサンプルアプリをビルドする`pom_sampleapp.xml`を用意しています。

### サンプルアプリのビルド

JAboutDialogは単体ではダイアログなので、呼び出すためのJFrameとサンプルとなる値を指定したサンプルアプリを同梱しています。サンプルアプリをビルドする場合は、以下のコマンドを実行して下さい。

```
$ git clone https://gitlab.com/utimukat55/jaboutdialog.git
$ cd jaboutdialog/jaboutdialog
$ mvn -f pom_sampleapp.xml package
```

コマンドを実行すると、`target`ディレクトリに`jaboutdialog-0.0.3-sample-app.jar`(実行可能jarファイル)が生成されます。`jaboutdialog-0.0.3.jar`は使用しません。

### サンプルの実行

以下のコマンドでサンプルを実行します。

```
$ java -jar target/jaboutdialog-0.0.3-sample-app.jar
```

![JAboutDialog Sample App](./samplemain1.png)

メニューから`Help`→`About`を選択します。

![Choose Help -> About](samplemain2.png)

`About this Software`ダイアログが`JAboutDialog`です。下部にある”Show System Property / Environment Variable"ボタンをクリックすると別ダイアログでシステムプロパティと環境変数の一覧を表示します。

![JAboutDialog](jaboutdialog.png)

![Environment dialog](environmentdialog.png)

### ライブラリjarのビルド

JAboutDialogをライブラリとして呼び出す場合に必要なjarファイルを生成する場合は、以下のコマンドを実行して下さい。

```
$ git clone https://gitlab.com/utimukat55/jaboutdialog.git
$ cd jaboutdialog/jaboutdialog
$ mvn package
```

コマンドを実行すると、`target`ディレクトリに`jaboutdialog-0.0.3.jar`が生成されます。

# 表示する情報

JAboutDialog は以下の情報を表示します。

- ロゴ画像 (64x64ピクセルで`javax.swing.ImageIcon`クラスで扱える画像を表示します。解像度が64x64でない場合は拡大・縮小をして表示します。)
- ソフトウェア名
- ソフトウェアバージョン
- ソフトウェアのURL
- ソフトウェアのライセンス
- ソフトウェアの著作権表示
- 使用しているライブラリの一覧 (`javax.swing.JTree`を使用しています)
- 貢献者の一覧

## 特記事項

![About License](license.png)

使用しているライブラリには以下の情報が必要です。ライセンスファイルのパスと著作権保持者の設定は任意ですが、CC0やPublic Domain Software等を除いた大半のライセンスでは再配布時にこれらの情報が必須のため、設定を強く推奨します。

- ライブラリ名
- URL (任意)
- ライブラリのバージョン (稀に同一ライブラリでもバージョンによってライセンスが変わることがある為、必須としています)
- ライセンス
- ライセンスファイルのパス (任意)
- 著作権保持者 (任意、複数指定可能)

貢献者には以下の情報が必要です。

- 名前
- URL (任意)

## 呼び出し方

`JDialog`を呼び出すように`JAboutDialog`を呼び出した後に`JAboutDialog#setInfo(Properties)`を使用してソフトウェア情報を設定して下さい。サンプルプログラムではXML形式のプロパティファイルをロードしています。

```java
JAboutDialog dialog = new JAboutDialog(f, true);
Properties prop = new Properties();
try (
  InputStream is = getClass().getResourceAsStream("/sampleinfo.xml")
) {
  prop.loadFromXML(is);
  dialog.setInfo(prop);
  dialog.setLocationRelativeTo(c);
  dialog.setVisible(true);
} catch (IOException e) {
  e.printStackTrace();
}
```

JAboutDialogのコンストラクタの第一引数である`f`はnullを指定可能です。表示位置を呼び出し元ダイアログに合わせるため、`JFrame`のインスタンスを指定しています。JAboutDialogはJDialogの全てのコンストラクタをオーバーライドしているため、任意のJAboutDialogのコンストラクタを呼び出すことができます。

JAboutDialog#setInfo() の引数は`java.util.Properties`なので、コード上でPropertiesクラスを生成や`.properties`ファイルをロードする等様々な方法で呼び出すことができます。

### 情報のフォーマット(Properties)

JAboutDialogでは以下のキーをPropertiesのインスタンスから取得します。

- SoftwareName
- SoftwareVersion
- SoftwareUrl
- SoftwareLicense
- SoftwareCopyright
- SoftwareIcon
- DependLibrary-n.name
- DependLibrary-n.version
- DependLibrary-n.license
- DependLibrary-n.url
- DependLibrary-n.licensepath
- DependLibrary-n.copyrightowner-n
- Contributor-n.name
- Contributor-n.url

DependLibrary と Contributor は1から始まるキーを使用し、複数指定可能です。(例： DependLibrary1, DependLibrary2, .., DependLibraryn). また、DependLibrary-n.copyrightownerも同様に1から始まるキーを使用し、複数指定可能です。サンプルで使用しているものは以下の内容です。 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
	<comment>Sample software info</comment>
	<entry key="SoftwareName">JAboutDialog</entry>
	<entry key="SoftwareVersion">0.0.1</entry>
	<entry key="SoftwareUrl">https://gitlab.com/utimukat55/jaboutdialog</entry>
	<!-- Common Licenses defined on com.gitlab.utimukat55.jaboutdialog.inner.CommonLicense.java 
		You can choose if described on it. -->
	<entry key="SoftwareLicense">CC0_1</entry>
	<entry key="SoftwareCopyright">This software and codes are licensed under CC0. https://creativecommons.org/publicdomain/zero/1.0/</entry>
 	<entry key="SoftwareIcon">/icon64.png</entry>
	<!-- DependLibrary n is read and will be listed (name, version and license are mandatory).-->
	<entry key="DependLibrary1.name">JTextComponent-Popup(example)</entry>
	<entry key="DependLibrary1.version">0.0.1</entry>
	<entry key="DependLibrary1.license">CC0_1</entry>
	<entry key="DependLibrary1.url">https://gitlab.com/utimukat55/jtextcomponent-popup</entry>

	<entry key="DependLibrary2.name">Maven Enforcer Plugin (only making jar)</entry>
	<entry key="DependLibrary2.version">3.0.0-M3</entry>
	<entry key="DependLibrary2.license">AL2</entry>
	<entry key="DependLibrary2.url">https://maven.apache.org/enforcer/maven-enforcer-plugin/</entry>
	<entry key="DependLibrary2.licensepath">license_maven_enforcer/ (in this sample, not include)</entry>
	<entry key="DependLibrary2.copyrightowner1">Copyright 2007-2013 The Apache Software Foundation</entry>

	<entry key="DependLibrary3.name">Apache Maven Compiler Plugin (only making jar)</entry>
	<entry key="DependLibrary3.version">3.8.1</entry>
	<entry key="DependLibrary3.license">AL2</entry>
	<entry key="DependLibrary3.url">https://maven.apache.org/plugins/maven-compiler-plugin/</entry>
	<entry key="DependLibrary3.licensepath">license_maven_compiler/ (in this sample, not include)</entry>
	<entry key="DependLibrary3.copyrightowner1">Copyright 2001-2019 The Apache Software Foundation</entry>

	<entry key="DependLibrary4.name">some library(example)</entry>
	<entry key="DependLibrary4.version">1.2.3</entry>
	<entry key="DependLibrary4.license">PROPRIETARY</entry>
	<entry key="DependLibrary4.licensepath">license_some_library/</entry>
	<entry key="DependLibrary4.copyrightowner1">Copyright (C) 2015 Bob</entry>
	<entry key="DependLibrary4.copyrightowner2">Copyright (C) 2021 Fictional works Inc.</entry>

	<!-- Contributor n is read and will be listed (name is mandatory). -->
	<entry key="Contributor1.name">utimukat55</entry>
	<entry key="Contributor1.url">http://gitlab.com/utimukat55</entry>
	
	<entry key="Contributor2.name">Alice</entry>
	<entry key="Contributor2.url">http://example.com/profile/alice</entry>
	
	<entry key="Contributor3.name">Bob</entry>
	<entry key="Contributor3.url">http://example.com/profile/bob</entry>
	
	<entry key="Contributor4.name">Charlie</entry>
	<entry key="Contributor5.name">Carol</entry>
	<entry key="Contributor6.name">Dave</entry>
</properties>
```

`SoftwareLicense`と`DependLibrary-n.license`は省略した表記を使用可能です。例えば "LGPL3PLUS"と指定した場合、JDialogは内部で保持するEnumから "GNU LESSER GENERAL PUBLIC LICENSE Version 3 or any later version" を取得し、置換して表示します。

# プロジェクトに取り込むには

## jarとして取り込む

JSR376(いわゆるJava9で導入されたモジュール)プロジェクトであってもそうでなくても、ライブラリとしても取り込み可能です。`jaboutdialog-0.0.2.jar`をライブラリとして取り込んで下さい。

モジュールとして取り込んだ場合、JAboutDialogは "com.gitlab.utimukat55.jaboutdialog" をモジュールとしてexportします。

## javaのソースとして取り込む

JAboutDialogはCC0でライセンスしているので、かなり多くの方法で使って頂くことができます。Javaのソースファイルとして取り込む場合、`com.gitlab.utimukat55.jaboutdialog`パッケージの以下のファイルをコピーして下さい。

- inner/CommonLicense.java
- inner/Constant.java
- inner/LogUtil.java
- inner/Util.java
- EnvironmentDialog.java
- JAboutDialog.java

`usage`パッケージはサンプルとして呼び出し元のクラス、`src/main/resources`ディレクトリはサンプルのXMLプロパティファイルとアイコンとライセンスが入っているため、サンプルを使用しない場合はこれらのファイルは不要です。

# プロジェクトの改善

もし使用しているライブラリやソフトウェアのライセンスが`inner/CommonLicense.java`にない場合、該当するライセンスとURLを教えて下さい。

# ライセンス

JAboutDialogはCC0で提供されます。([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed))



# その他サポート記事

こちらをご覧下さい。適宜追加していきます。

https://kasu-kasu.ga/tags/jaboutdialog/
