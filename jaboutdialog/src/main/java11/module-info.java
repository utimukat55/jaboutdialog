/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

module com.gitlab.utimukat55.jaboutdialog {
	requires transitive java.desktop;
	requires transitive java.logging;
	exports com.gitlab.utimukat55.jaboutdialog;
}
