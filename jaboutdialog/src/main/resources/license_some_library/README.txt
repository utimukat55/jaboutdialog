Copyright (C) 2015 Bob
Copyright (C) 2021 Fictional works Inc.

License of some library

This is example license file of "some library".

Most of cases, license file must announce to end user. This example file is in src/main/resources/licenses , please locate these files near the output file when distributing products.

In the JAboutDialog, license file path can describe on each license. To avoid trouble with using useful libraries, please specify license file path.