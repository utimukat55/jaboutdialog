/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.inner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class LogUtil {
	private LogUtil() {}
	
	public static final String LOG_FORMAT_KEY = "java.util.logging.SimpleFormatter.format";
	public static final String LOG_FORMAT_VALUE = "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$s %2$s %5$s%6$s%n";

	public static final String LOG_CONSOLE_HANDLER = "handlers=java.util.logging.ConsoleHandler";
	public static final String LOG_CONSOLE_LEVEL_KEY = "java.util.logging.ConsoleHandler.level";
	public static final String LOG_CONSOLE_LEVEL_VALUE = "ALL";
	public static final String LOG_CONSOLE_PROPERTY = LOG_CONSOLE_HANDLER + "\n" + LOG_CONSOLE_LEVEL_KEY + "=" + LOG_CONSOLE_LEVEL_VALUE;

	public static void improveLogFormat() {
		System.setProperty(LOG_FORMAT_KEY, LOG_FORMAT_VALUE);
	}
	
	public static void setConsoleLogAll() {
		try (InputStream is = new ByteArrayInputStream(LOG_CONSOLE_PROPERTY.getBytes("UTF-8"))) {
		    try {
	            LogManager.getLogManager().readConfiguration(is);
			} catch (IOException e) {
				Logger.getGlobal().log(Level.WARNING, "IOException caught when getLogManager().readConfiguration()", e);
			}
	    } catch (UnsupportedEncodingException e) {
	    	Logger.getGlobal().log(Level.SEVERE, "please check encoding", e);
		} catch (IOException e) {
			Logger.getGlobal().log(Level.SEVERE, "IOException caught when BAIS constructor", e);
		}
	}
}
