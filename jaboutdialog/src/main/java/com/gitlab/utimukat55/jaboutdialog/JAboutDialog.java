/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.gitlab.utimukat55.jaboutdialog.inner.Constant;
import com.gitlab.utimukat55.jaboutdialog.inner.Util;

public class JAboutDialog extends JDialog {

	private static final long serialVersionUID = 6852257167390401532L;

	private static final Logger logger = Logger.getLogger(JAboutDialog.class.getName());

	private Dialog d = this;
	private Component c = this;
	private JLabel lblSoftwareName;
	private JLabel lblSoftwareVersion;
	private JLabel lblSoftwareUrl;
	private JLabel lblSoftwareLicense;
	private JTree treeDependLibrary;
	private JTextPane txtContributors;
	private JTextArea txtSoftwareCopyright;
	private JLabel lblIcon;
	private JPanel panelIcon;
	private JScrollPane scrollLibrary;
	private JScrollPane scrollContributor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		try {
			JAboutDialog dialog = new JAboutDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			logger.log(Level.WARNING, "failed to call JDialog#setVisible()", e);
		}
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	private void createLayout() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		setTitle("About this Software");
		setBounds(100, 100, 450, 493);
		
		Color forNimbusColor = new Color(0,0,0,0);
		JButton btnInfoDialog = new JButton("Show System Property / Environment Variable");
		JLabel lblLibrary = new JLabel("Using libraries:");
		JLabel lblContributor = new JLabel("Contributors:");
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		
		panelIcon = new JPanel();
		lblSoftwareName = new JLabel("Placeholder(Software name)");
		lblSoftwareVersion = new JLabel("Placeholder(Software Version)");
		lblSoftwareUrl = new JLabel("Placeholder(Software URL)");
		lblSoftwareLicense = new JLabel("Placeholder(Software License)");
		txtSoftwareCopyright = new JTextArea();
		scrollLibrary = new JScrollPane();
		scrollContributor = new JScrollPane();
		treeDependLibrary = new JTree();
		txtContributors = new JTextPane();
		lblIcon = new JLabel("No Image");
		
		btnInfoDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
				// show System Properties/Environment Variables Dialog.
				EnvironmentDialog dialog = new EnvironmentDialog(d, true);
				dialog.setLocationRelativeTo(c);
				dialog.setVisible(true);
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
			}
		});

		txtSoftwareCopyright.setWrapStyleWord(true);
		txtSoftwareCopyright.setLineWrap(true);
		txtSoftwareCopyright.setText("Placeholder(Software copyright)");
		txtSoftwareCopyright.setOpaque(false);
		txtSoftwareCopyright.setEditable(false);
		txtSoftwareCopyright.setFocusable(false);
		// for Nimbus L&F https://bugs.java.com/bugdatabase/view_bug.do?bug_id=6687960
		txtSoftwareCopyright.setBorder(BorderFactory.createEmptyBorder());
		txtSoftwareCopyright.setBackground(forNimbusColor);

		txtContributors.setText("Placeholder(contributors)");
		txtContributors.setOpaque(false);
		txtContributors.setEditable(false);
		txtContributors.setFocusable(false);
		// for Nimbus L&F https://bugs.java.com/bugdatabase/view_bug.do?bug_id=6687960
		txtContributors.setBorder(BorderFactory.createEmptyBorder());
		txtContributors.setBackground(forNimbusColor);
		txtContributors.setContentType("text/html");
		txtContributors.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent event) {
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
				if (HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType())) {
					try {
						Desktop.getDesktop().browse(event.getURL().toURI());
					} catch (URISyntaxException | IOException e) {
						logger.log(Level.WARNING, "couldn't open url", e);
					} catch (UnsupportedOperationException e) {
						logger.log(Level.WARNING, "Desktop#browse() doesn't supported.", e);
					} catch (IllegalArgumentException e) {
						logger.log(Level.WARNING, "URI couldn't convert to URL", e);
					} catch (Exception e) {
						logger.log(Level.WARNING, "Unexpected Exception.", e);
					}
				}
				logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
			}
		});

		treeDependLibrary.setRootVisible(false);
		scrollLibrary.setViewportView(treeDependLibrary);
		scrollContributor.setViewportView(txtContributors);
		panelIcon.add(lblIcon);

		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(70).addComponent(btnInfoDialog))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout
								.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtSoftwareCopyright, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
										.addComponent(panelIcon, GroupLayout.PREFERRED_SIZE, 64,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblSoftwareLicense).addComponent(lblSoftwareUrl)
												.addComponent(lblSoftwareVersion).addComponent(lblSoftwareName)))
								.addComponent(lblLibrary, Alignment.LEADING)
								.addComponent(lblContributor, Alignment.LEADING)
								.addComponent(scrollContributor, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 410,
										Short.MAX_VALUE)
								.addComponent(scrollLibrary, GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE))))
				.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addComponent(lblSoftwareName)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblSoftwareVersion)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblSoftwareUrl)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblSoftwareLicense))
								.addComponent(panelIcon, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addComponent(txtSoftwareCopyright, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblLibrary)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scrollLibrary, GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblContributor)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scrollContributor, GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE).addGap(26)
						.addComponent(btnInfoDialog).addGap(20)));

		getContentPane().setLayout(groupLayout);
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	/**
	 * set informations to create JAboutDialog.
	 * @param info informations
	 * @throws IllegalArgumentException info is null
	 */
	public void setInfo(Properties info) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");

		if (info == null) {
			logger.severe("argument is null. throw Exception.");
			throw new IllegalArgumentException("argument is null.");
		}
		lblSoftwareName.setText(info.getProperty(Constant.SOFTWARENAME));
		lblSoftwareVersion.setText(info.getProperty(Constant.SOFTWAREVERSION));
		String url = info.getProperty(Constant.SOFTWAREURL);
		if (url == null) {
			logger.warning("failed to load url from Properties(undefined):[" + Constant.SOFTWAREURL + "]");
		} else {
			lblSoftwareUrl.setText(Util.createHyperlink(url));
			lblSoftwareUrl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			lblSoftwareUrl.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent event) {
					try {
						Desktop.getDesktop().browse(new URI(url));
					} catch (URISyntaxException | IOException e) {
						logger.log(Level.WARNING, "couldn't open url", e);
					} catch (UnsupportedOperationException e) {
						logger.log(Level.WARNING, "Desktop#browse() doesn't supported.", e);
					} catch (IllegalArgumentException e) {
						logger.log(Level.WARNING, "URI couldn't convert to URL", e);
					} catch (Exception e) {
						logger.log(Level.WARNING, "Unexpected Exception.", e);
					}
				}
			});
		}
		String license = info.getProperty(Constant.SOFTWARELICENSE);
		if (license == null) {
			logger.warning("failed to load license from Properties(undefined):[" + Constant.SOFTWARELICENSE + "]");
		} else {
			lblSoftwareLicense.setText(Util.getLicenseFromEnum(license));
		}
		txtSoftwareCopyright.setText(info.getProperty(Constant.SOFTWARECOPYRIGHT));

		panelIcon.removeAll();
		try {
			String iconUrl = info.getProperty(Constant.SOFTWAREICON);
			if (iconUrl == null) {
				logger.warning("failed to load icon from Properties(undefined):[" + Constant.SOFTWAREICON + "]");
			} else {
				URL iconUrls = getClass().getResource(iconUrl);
				ImageIcon icon = new ImageIcon(iconUrls);
				JLabel lblIcon = new JLabel(Util.fixImageIcon(icon, 64));
				panelIcon.add(lblIcon);
			}
		} catch (NullPointerException e) {
			logger.log(Level.SEVERE, "failed to load icon from Properties:[" + info.getProperty(Constant.SOFTWAREICON) + "]", e);
		}

		// Depend libraries
		DefaultTreeModel model = (DefaultTreeModel)treeDependLibrary.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
		root.removeAllChildren();
		for (DefaultMutableTreeNode node : Util.createDependsLibraryFromProperties(info)) {
			root.add(node);
		}
		model.reload();

		// Contributors
		txtContributors.setText(Util.createContributorFromProperties(info));
		txtContributors.setCaretPosition(0);
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
	}

	// Constructors

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog() {
		super();
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Frame owner) {
		super(owner);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Frame owner, boolean modal) {
		super(owner, modal);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Frame owner, String title) {
		super(owner, title);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Dialog owner) {
		super(owner);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Dialog owner, boolean modal) {
		super(owner, modal);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Dialog owner, String title) {
		super(owner, title);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Window owner) {
		super(owner);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Window owner, ModalityType modalityType) {
		super(owner,modalityType);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Window owner, String title) {
		super(owner, title);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Window owner, String title, Dialog.ModalityType modalityType) {
		super(owner, title, modalityType);
		createLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	public JAboutDialog(Window owner, String title, Dialog.ModalityType modalityType, GraphicsConfiguration gc) {
		super(owner, title, modalityType, gc);
		createLayout();
	}
}
