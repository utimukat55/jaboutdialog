/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.inner;

public enum CommonLicense {

	// The Unlicense
	UNLICENSE("Unlicense"),
	// CC0 1.0
	CC0_1("CC0 1.0 Universal (CC0 1.0)"),
	// CC BY 1.0 2.0 2.5 3.0 4.0
	CCBY_1("Attribution 1.0 Generic (CC BY 1.0)"),
	CCBY_2("Attribution 2.0 Generic (CC BY 2.0)"),
	CCBY_2_5("Attribution 2.5 Generic (CC BY 2.5)"),
	CCBY_3("Attribution 3.0 Unported (CC BY 3.0)"),
	CCBY_4("Attribution 4.0 International (CC BY 4.0)"),
	// CC BY-SA 1.0 2.0 2.5 3.0 4.0
	CCBYSA_1("Attribution-ShareAlike 1.0 Generic (CC BY-SA 1.0)"),
	CCBYSA_2("Attribution-ShareAlike 2.0 Generic (CC BY-SA 2.0)"),
	CCBYSA_2_5("Attribution-ShareAlike 2.5 Generic (CC BY-SA 2.5)"),
	CCBYSA_3("Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)"),
	CCBYSA_4("Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)"),
	// CC BY-NC 1.0 2.0 2.5 3.0 4.0
	CCBYNC_1("Attribution-NonCommercial 1.0 Generic (CC BY-NC 1.0)"),
	CCBYNC_2("Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0)"),
	CCBYNC_2_5("Attribution-NonCommercial 2.5 Generic (CC BY-NC 2.5)"),
	CCBYNC_3("Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)"),
	CCBYNC_4("Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)"),
	// CC BY-NC-SA 1.0 2.0 2.5 3.0 4.0
	CCBYNCSA_1("Attribution-NonCommercial-ShareAlike 1.0 Generic (CC BY-NC-SA 1.0)"),
	CCBYNCSA_2("Attribution-NonCommercial-ShareAlike 2.0 Generic (CC BY-NC-SA 2.0)"),
	CCBYNCSA_2_5("Attribution-NonCommercial-ShareAlike 2.5 Generic (CC BY-NC-SA 2.5)"),
	CCBYNCSA_3("Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)"),
	CCBYNCSA_4("Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)"),
	// CC BY-ND 1.0 2.0 2.5 3.0 4.0
	CCBYND_1("Attribution-NoDerivs 1.0 Generic (CC BY-ND 1.0)"),
	CCBYND_2("Attribution-NoDerivs 2.0 Generic (CC BY-ND 2.0)"),
	CCBYND_2_5("Attribution-NoDerivs 2.5 Generic (CC BY-ND 2.5)"),
	CCBYND_3("Attribution-NoDerivs 3.0 Unported (CC BY-ND 3.0)"),
	CCBYND_4("Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0)"),
	// CC BY-NC-ND 1.0 2.0 2.5 3.0 4.0
	CCBYNCND_1("Attribution-NoDerivs-NonCommercial 1.0 Generic (CC BY-ND-NC 1.0)"),
	CCBYNCND_2("Attribution-NonCommercial-NoDerivs 2.0 Generic (CC BY-NC-ND 2.0)"),
	CCBYNCND_2_5("Attribution-NonCommercial-NoDerivs 2.5 Generic (CC BY-NC-ND 2.5)"),
	CCBYNCND_3("Attribution-NonCommercial-NoDerivs 3.0 Unported (CC BY-NC-ND 3.0)"),
	CCBYNCND_4("Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)"),
	// MIT
	MIT("The MIT License"),
	// X11
	X11("X11 License"),
	// 4BSD
	BSD4("The original BSD license(4-clause)"),
	// 3BSD
	BSD3("New BSD License(3-clause)"),
	// 2BSD
	BSD2("Simplified BSD License(2-clause)"),
	// 0BSD
	BSD0("BSD Zero Clause License(0-clause)"),
	// AL2.0
	AL2("APACHE LICENSE, VERSION 2.0"),
	// ASL1.1
	ASL1_1("The Apache Software License, Version 1.1"),
	// ASL1.0
	ASL1("Apache License, Version 1.0 "),
	// LGPL2.0 LGPL2.0+
	LGPL2("GNU LIBRARY GENERAL PUBLIC LICENSE Version 2"),
	LGPL2PLUS("GNU LIBRARY GENERAL PUBLIC LICENSE Version 2 or any later version"),
	// LGPL2.1 LGPL2.1+
	LGPL2_1("GNU LESSER GENERAL PUBLIC LICENSE Version 2.1"),
	LGPL2_1PLUS("GNU LESSER GENERAL PUBLIC LICENSE Version 2.1 or any later version"),
	// LGPL3.0 LGPL3.0+
	LGPL3("GNU LESSER GENERAL PUBLIC LICENSE Version 3"),
	LGPL3PLUS("GNU LESSER GENERAL PUBLIC LICENSE Version 3 or any later version"),
	// GPL1.0 GPL1.0+
	GPL1("GNU GENERAL PUBLIC LICENSE Version 1"),
	GPL1PLUS("GNU GENERAL PUBLIC LICENSE Version 1 or any later version"),
	// GPL2.0 GPL2.0+
	GPL2("GNU GENERAL PUBLIC LICENSE Version 2"),
	GPL2PLUS("GNU GENERAL PUBLIC LICENSE Version 2 or any later version"),
	// GPL3.0 GPL3.0+
	GPL3("GNU GENERAL PUBLIC LICENSE Version 3"),
	GPL3PLUS("GNU GENERAL PUBLIC LICENSE Version 3 or any later version"),
	// AGPL3.0 AGPL3.0+
	AGPL3("GNU AFFERO GENERAL PUBLIC LICENSE Version 3"),
	AGPL3PLUS("GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or any later version"),
	// GFDL1.1 GFDL1.1+ GFDL1.2 GFDL1.2+ GFDL1.3 GFDL1.3+
	GFDL1_1("GNU Free Documentation License Version 1.1"),
	GFDL1_1PLUS("GNU Free Documentation License Version 1.1 or any later version"),
	GFDL1_2("GNU Free Documentation License Version 1.2"),
	GFDL1_2PLUS("GNU Free Documentation License Version 1.2 or any later version"),
	GFDL1_3("GNU Free Documentation License Version 1.3"),
	GFDL1_3PLUS("GNU Free Documentation License Version 1.3 or any later version"),
	// AFL1.2 AFL2.1 AFL3.0
	AFL1_1("Academic Free License v1.1"),
	AFL1_2("Academic Free License v1.2"),
	AFL_2("Academic Free License v2.0"),
	AFL_2_1("Academic Free License v2.1"),
	AFL_3("Academic Free License v3.0"),
	// Artistic License 1.0 Artistic License 2.0
	ARTISTICLICENSE1("Artistic License 1.0"),
	ARTISTICLICENSE2("Artistic License 2.0"),
	// Boost Software License 1.0
	BOOST_1("Boost Software License 1.0"),
	// EPL1.0 EPL2.0
	EPL1("Eclipse Public License 1.0"),
	EPL2("Eclipse Public License 2.0"),
	// MPL1.0 MPL1.1 MPL2.0
	MPL1("Mozilla Public License 1.0"),
	MPL1_1("Mozilla Public License 1.1"),
	MPL2("Mozilla Public License 2.0"),
	// CDDL 1.0 CDDL 1.1
	CDDL1("Common Development and Distribution License 1.0"),
	CDDL1_1("Common Development and Distribution License 1.1"),
	// EUPL 1.0 EUPL1.1 EUPL1.2
	EUPL1("European Union Public License 1.0"),
	EUPL1_1("European Union Public License 1.1"),
	EUPL1_2("European Union Public License 1.2"),
	// ISC License
	ISC("ISC License"),
	// Ms-PL
	MS_PL("Microsoft Public License"),
	// PostgreSQL License
	POSTGRES("PostgreSQL License"),
	// WTFPL
	WTFPL("Do What The F*ck You Want To Public License"),
	// zlib License
	ZLIB("zlib License"),
	// SIL 1.0 SIL 1.1
	SIL1("SIL Open Font License 1.0"),
	SIL1_1("SIL Open Font License 1.1"),
	// IPA
	IPA("IPA Font License"),
	// Public domain
	PUBLICDOMAIN("Public Domain"),
	// Proprietary
	PROPRIETARY("Proprietary"),
	;
	
	private String description;
	private CommonLicense(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	
}
