/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.usage;

/**
 * Entry point.
 * 
 * @author utimukat55
 */
public class App {
	public static void main(String[] args) {
		SampleMainFrame.main(args);
	}
}
