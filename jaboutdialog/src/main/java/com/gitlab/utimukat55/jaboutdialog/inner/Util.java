/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.inner;

import java.awt.Image;
import java.awt.Label;
import java.awt.MediaTracker;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

public class Util {

	private Util() {}
	private static final Logger logger = Logger.getLogger(Util.class.getName());
	
	public static String[][] convertMapToStrings(Map<String, String> sourceMap) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		return sourceMap
				.entrySet()
				.stream()
				.map(entry -> new String[] { entry.getKey(), entry.getValue() })
				.toArray(String[][]::new);
	}
	
	public static TreeMap<String, String> createEnvironmentVariable() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		return new TreeMap<String, String>(System.getenv());
	}

	public static TreeMap<String, String> createSystemProperty() {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		TreeMap<String, String> sortedSystemProperty = new TreeMap<String, String>();
		Properties systemProperty = System.getProperties();
		for(Entry<Object, Object> entry : systemProperty.entrySet()) {
			sortedSystemProperty.put((String)entry.getKey(), (String)entry.getValue());
		}
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return sortedSystemProperty;
	}
	
	public static String getLicenseFromEnum(String propString) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		
		CommonLicense license;
		try {
			license = CommonLicense.valueOf(propString);
			return license.getDescription();
		} catch (IllegalArgumentException e) {
			// not defined license.
			logger.log(Level.INFO, "Undefined license was specified. Use direct value:[" + propString + "]", e);
			return propString;
		}
	}
	
	public static List<DefaultMutableTreeNode> createDependsLibraryFromProperties(Properties props) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		List<DefaultMutableTreeNode> root = new ArrayList<>();
		int num = 1;
		
		while (true) {
			String keyName = Constant.DEPENDLIBRARY_PREFIX + num + Constant.DEPENDLIBRARYNAME_SUFFIX;
			String keyVersion = Constant.DEPENDLIBRARY_PREFIX + num + Constant.DEPENDLIBRARYVERSION_SUFFIX;
			String keyLicense = Constant.DEPENDLIBRARY_PREFIX + num + Constant.DEPENDLIBRARYLICENSE_SUFFIX;
			String keyUrl = Constant.DEPENDLIBRARY_PREFIX + num + Constant.DEPENDLIBRARYURL_SUFFIX;
			String keyLicensePath = Constant.DEPENDLIBRARY_PREFIX + num + Constant.DEPENDLIBRARYLICENSEPATH_SUFFIX;
			String name = props.getProperty(keyName);
			String version = props.getProperty(keyVersion);
			String license = props.getProperty(keyLicense);
			String url = props.getProperty(keyUrl);
			String licensePath = props.getProperty(keyLicensePath);
			if (name == null || version == null || license == null) {
				break;
			}
			DefaultMutableTreeNode nameNode = new DefaultMutableTreeNode(name);
			logger.config("name:[" + name + "]/version:[" + version + "]/license:[" + license + "]");
			if (url != null) {
				logger.config("library url found.[" + url + "]");
				DefaultMutableTreeNode urlNode = new DefaultMutableTreeNode(url);
				nameNode.add(urlNode);
			}
			DefaultMutableTreeNode versionNode = new DefaultMutableTreeNode("Version: " + version);
			nameNode.add(versionNode);
			DefaultMutableTreeNode licenseNode = new DefaultMutableTreeNode(getLicenseFromEnum(license));
			nameNode.add(licenseNode);
			if (licensePath != null) {
				logger.config("library path found.[" + licensePath + "]");
				DefaultMutableTreeNode licensePathNode = new DefaultMutableTreeNode("License path: " + licensePath);
				nameNode.add(licensePathNode);
			}
			for (String copyrightOwner : createCopyrightOwners(props, num)) {
				logger.config("copyright owner add.[" + copyrightOwner + "]");
				DefaultMutableTreeNode copyrightOwnerNode = new DefaultMutableTreeNode(copyrightOwner);
				nameNode.add(copyrightOwnerNode);
			}
			root.add(nameNode);
			num++;
		}
		logger.config("libraries:[" + num + "]");
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return root;
	}
	
	private static List<String> createCopyrightOwners(Properties props, int licenseIndex) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		List<String> root = new ArrayList<>();
		int num = 1; 
		while (true) {
			String keyCopyrightOwner = Constant.DEPENDLIBRARY_PREFIX + licenseIndex + Constant.DEPENDLIBRARYCOPYRIGHTOWNER_SUFFIX + num;
			String copyrightOwner = props.getProperty(keyCopyrightOwner);
			if (copyrightOwner == null) {
				break;
			}
			root.add(copyrightOwner);
			num++;
		}
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return root;
	}
	
	public static String createContributorFromProperties(Properties props) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		StringBuilder sb = new StringBuilder();
		int num = 1;
		
		while (true) {
			String keyName = Constant.CONTRIBUTOR_PREFIX + num + Constant.CONTRIBUTORNAME_SUFFIX;
			String keyUrl = Constant.CONTRIBUTOR_PREFIX + num + Constant.CONTRIBUTORURL_SUFFIX;
			String name = props.getProperty(keyName);
			String url = props.getProperty(keyUrl);
			if (name == null) {
				break;
			}
			logger.config("name:[" + name + "]/url:[" + url + "]");
			sb.append(markUpContributor(name, url));
			num++;
		}
		logger.config("contributors:[" + num + "]");
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return sb.toString();
	}
	
	private static String markUpContributor(String name, String url) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		if (url != null) {
			sb.append(" ");
			boolean isWeb = false;
			if (url.startsWith("https://") || url.startsWith("http://")) {
				isWeb = true;
			}
			if (isWeb) {
				sb.append("<a href=\"");
				sb.append(url);
				sb.append("\">");
			}
			sb.append(url);
			if (isWeb) {
				sb.append("</a>");
			}
		}
		sb.append("<br>");
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return sb.toString();
	}
	
	public static ImageIcon fixImageIcon(ImageIcon src, int sideLength) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		if (src.getIconHeight() == sideLength && src.getIconWidth() == sideLength) {
			// no need to fix
			return src;
		}
		
		Label trackerObject = new Label();
		MediaTracker tracker = new MediaTracker(trackerObject);
		double expandRate = getExpandRate(src, sideLength);
		
		Image fixedImage = src.getImage().getScaledInstance((int)(src.getIconWidth() * expandRate), (int)(src.getIconHeight() * expandRate), Image.SCALE_DEFAULT);
		tracker.addImage(fixedImage, 1);
		ImageIcon fixedIcon = new ImageIcon(fixedImage);
		try {
			tracker.waitForAll();
		} catch (InterruptedException e) {
			logger.log(Level.WARNING, "Interruped during convert icon. Result is not guaranteed.", e);
		}
		
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return fixedIcon;
	}
	
	private static double getExpandRate(ImageIcon src, int sideLength) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		double expandRate = 0;
		int srcHeight = src.getIconHeight();
		int srcWidth = src.getIconWidth();
		logger.config("srcHeight:[" + srcHeight + "]/srcWidth:[" + srcWidth + "]/sideLength:[" + sideLength + "]");

		if (srcHeight >= sideLength && srcWidth >= sideLength) {
			// shrink
			expandRate = (double)sideLength / (double)Math.max(srcHeight, srcWidth);
		} else {
			// expand
			expandRate = (double)sideLength / (double)Math.max(srcHeight, srcWidth);
		}

		logger.config("expandRate:[" + expandRate + "]");
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() END");
		return expandRate;
	}
	
	public static String createHyperlink(String url) {
		logger.fine(new Object() {}.getClass().getEnclosingMethod().getName() + "() BGN");
		return "<html><a href=\"" + url + "\">" + url + "</a></html>";
	}
}
