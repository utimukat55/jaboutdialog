/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.usage;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.EmptyBorder;

import com.gitlab.utimukat55.jaboutdialog.JAboutDialog;
import com.gitlab.utimukat55.jaboutdialog.inner.LogUtil;

public class SampleMainFrame extends JFrame {

	private static final long serialVersionUID = 4239085217750453007L;
	private JPanel contentPane;

	private Component c = this;
	private Frame f = this;
	
	public static void main(String[] args) {
		LogUtil.improveLogFormat();
		LogUtil.setConsoleLogAll();

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			Logger.getGlobal().log(Level.WARNING, "failed to set Nimbus LnF", e);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SampleMainFrame frame = new SampleMainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					Logger.getGlobal().log(Level.SEVERE, "Exception in run()", e);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SampleMainFrame() {
		setTitle("JAboutDialog Sample App");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menuHelp = new JMenu("Help");
		menuBar.add(menuHelp);
		
		
		JMenuItem menuItemAbout = new JMenuItem("About");
		menuItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JAboutDialog dialog = new JAboutDialog(f, true);
				Properties prop = new Properties();
				try (
					InputStream is = getClass().getResourceAsStream("/sampleinfo.xml")
				) {
					prop.loadFromXML(is);
					dialog.setInfo(prop);

					dialog.setLocationRelativeTo(c);
					dialog.setVisible(true);
				} catch (IOException e) {
					Logger.getGlobal().log(Level.SEVERE, "failed to read Properties", e);
				}
			}
		});
		menuHelp.add(menuItemAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		menuHelp.setMnemonic(KeyEvent.VK_H);
		menuItemAbout.setMnemonic(KeyEvent.VK_A);
		
		JPanel panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(202, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		);
		contentPane.setLayout(gl_contentPane);
		
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.setBorder(new LineBorder(SystemColor.controlShadow, 1, false));
		
		JLabel lblStatus = new JLabel("Please choose Help -> About");
		panel.add(lblStatus);
	}
}
