package com.gitlab.utimukat55.jaboutdialog;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EnvironmentDialogTest {

	@Test
	@DisplayName("Normal")
	void testSetVisible1() {
		EnvironmentDialog d = new EnvironmentDialog();
		assertDoesNotThrow(() -> d.setVisible(true));
	}

}
