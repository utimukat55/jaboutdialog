/**
 * JAboutDialog
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
package com.gitlab.utimukat55.jaboutdialog.inner;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.swing.ImageIcon;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UtilTest {

	@Test
	@DisplayName("Convert Map to Array")
	void testConvertMapToStrings() {
		Map<String, String> t = new TreeMap<String, String>();
		t.put("key", "value");
		String[][] result = Util.convertMapToStrings(t);
		assertNotNull(result);
		assertEquals("key", result[0][0]);
		assertEquals("value", result[0][1]);
	}
	
	@Test
	@DisplayName("Create TreeMap env")
	void testCreateEnvironmentVariable() {
		assertNotNull(Util.createEnvironmentVariable());
	}

	@Test
	@DisplayName("Create TreeMap sysprop")
	void testCreateSystemProperty() {
		assertNotNull(Util.createSystemProperty());
	}

	@Test
	@DisplayName("Get defined license from Enum.")
	void testGetLicenseFromEnum1() {
		assertEquals(CommonLicense.valueOf("MIT").getDescription(), Util.getLicenseFromEnum("MIT"));
	}

	@Test
	@DisplayName("Get undefined license from Enum.")
	void testGetLicenseFromEnum2() {
		assertEquals("foobar", Util.getLicenseFromEnum("foobar"));
	}

	@Test
	@DisplayName("Get markuped String.(no source)")
	void testCreateContributorFromProperties1() {
		assertEquals("", Util.createContributorFromProperties(createTestProperty1()));
	}
	
	Properties createTestProperty1() {
		Properties p = new Properties();
		return p;
	}
	
	@Test
	@DisplayName("Get markuped String.(only name)")
	void testCreateContributorFromProperties2() {
		assertEquals("Bob<br>", Util.createContributorFromProperties(createTestProperty2()));
	}
	
	Properties createTestProperty2() {
		Properties p = new Properties();
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORNAME_SUFFIX, "Bob");
		return p;
	}
	
	@Test
	@DisplayName("Get markuped String.(name and url(http))")
	void testCreateContributorFromProperties3() {
		assertEquals("Bob <a href=\"http://a.b/\">http://a.b/</a><br>", Util.createContributorFromProperties(createTestProperty3()));
	}
	
	Properties createTestProperty3() {
		Properties p = new Properties();
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORNAME_SUFFIX, "Bob");
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORURL_SUFFIX, "http://a.b/");
		return p;
	}

	@Test
	@DisplayName("Get markuped String.(name and url(https))")
	void testCreateContributorFromProperties4() {
		assertEquals("Bob <a href=\"https://a.b/\">https://a.b/</a><br>", Util.createContributorFromProperties(createTestProperty4()));
	}
	
	Properties createTestProperty4() {
		Properties p = new Properties();
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORNAME_SUFFIX, "Bob");
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORURL_SUFFIX, "https://a.b/");
		return p;
	}

	@Test
	@DisplayName("Get markuped String.(name and not url)")
	void testCreateContributorFromProperties5() {
		assertEquals("Bob @a<br>", Util.createContributorFromProperties(createTestProperty5()));
	}
	
	Properties createTestProperty5() {
		Properties p = new Properties();
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORNAME_SUFFIX, "Bob");
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORURL_SUFFIX, "@a");
		return p;
	}

	@Test
	@DisplayName("Get markuped String.(2 contributors)")
	void testCreateContributorFromProperties6() {
		assertEquals("Bob<br>Alice<br>", Util.createContributorFromProperties(createTestProperty6()));
	}
	
	Properties createTestProperty6() {
		Properties p = new Properties();
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 1 + Constant.CONTRIBUTORNAME_SUFFIX, "Bob");
		p.setProperty(Constant.CONTRIBUTOR_PREFIX + 2 + Constant.CONTRIBUTORNAME_SUFFIX, "Alice");
		return p;
	}
	
	@Test
	@DisplayName("Create icon(no fix)")
	void testFixImageIcon1() {
		ImageIcon icon = createTestImage1();
		ImageIcon iconTest = Util.fixImageIcon(icon, 64);
		assertEquals(icon, iconTest);
	}
	
	ImageIcon createTestImage1() {
		Image img = (Image)(new BufferedImage(64, 64, BufferedImage.TYPE_INT_RGB));
		return new ImageIcon(img);
	}
	
	@Test
	@DisplayName("Create icon(shrink(width is long))")
	void testFixImageIcon2() {
		ImageIcon icon = createTestImage2();
		ImageIcon iconTest = Util.fixImageIcon(icon, 64);
		int iconTestWidth = iconTest.getIconWidth();
		int iconTestHeight = iconTest.getIconHeight();
		assertEquals(64, iconTestWidth);
		assertEquals(32, iconTestHeight);
	}
	
	ImageIcon createTestImage2() {
		Image img = (Image)(new BufferedImage(128, 64, BufferedImage.TYPE_INT_RGB));
		return new ImageIcon(img);
	}
	
	@Test
	@DisplayName("Create icon(shrink(height is long))")
	void testFixImageIcon3() {
		ImageIcon icon = createTestImage3();
		ImageIcon iconTest = Util.fixImageIcon(icon, 64);
		int iconTestWidth = iconTest.getIconWidth();
		int iconTestHeight = iconTest.getIconHeight();
		assertEquals(32, iconTestWidth);
		assertEquals(64, iconTestHeight);
	}
	
	ImageIcon createTestImage3() {
		Image img = (Image)(new BufferedImage(128, 256, BufferedImage.TYPE_INT_RGB));
		return new ImageIcon(img);
	}
	
	@Test
	@DisplayName("Create icon(expand(height is long))")
	void testFixImageIcon4() {
		ImageIcon icon = createTestImage4();
		ImageIcon iconTest = Util.fixImageIcon(icon, 64);
		int iconTestWidth = iconTest.getIconWidth();
		int iconTestHeight = iconTest.getIconHeight();
		assertEquals(32, iconTestWidth);
		assertEquals(64, iconTestHeight);
	}
	
	ImageIcon createTestImage4() {
		Image img = (Image)(new BufferedImage(16, 32, BufferedImage.TYPE_INT_RGB));
		return new ImageIcon(img);
	}
	
	@Test
	@DisplayName("Create icon(expand(width is long))")
	void testFixImageIcon5() {
		ImageIcon icon = createTestImage5();
		ImageIcon iconTest = Util.fixImageIcon(icon, 64);
		int iconTestWidth = iconTest.getIconWidth();
		int iconTestHeight = iconTest.getIconHeight();
		assertEquals(64, iconTestWidth);
		assertEquals(32, iconTestHeight);
	}
	
	ImageIcon createTestImage5() {
		Image img = (Image)(new BufferedImage(16, 8, BufferedImage.TYPE_INT_RGB));
		return new ImageIcon(img);
	}
	
	@Test
	@DisplayName("Create TreeNode by Properties(0)")
	void testCreateDependsLibraryFromProperties1() {
		assertNotNull(Util.createDependsLibraryFromProperties(new Properties()));
	}
	
	@Test
	@DisplayName("Create TreeNode by Properties(1, no URL)")
	void testCreateDependsLibraryFromProperties2() {
		assertNotNull(Util.createDependsLibraryFromProperties(createTestProperty7()));
	}
	
	Properties createTestProperty7() {
		Properties p = new Properties();
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYNAME_SUFFIX, "lib1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYVERSION_SUFFIX, "ver1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYLICENSE_SUFFIX, "GPL2");
		return p;
	}

	@Test
	@DisplayName("Create TreeNode by Properties(2)")
	void testCreateDependsLibraryFromProperties3() {
		assertNotNull(Util.createDependsLibraryFromProperties(createTestProperty8()));
	}
	
	Properties createTestProperty8() {
		Properties p = new Properties();
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYNAME_SUFFIX, "lib1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYVERSION_SUFFIX, "ver1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYLICENSE_SUFFIX, "GPL2");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYURL_SUFFIX, "url1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYNAME_SUFFIX, "lib2");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYVERSION_SUFFIX, "ver2");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYLICENSE_SUFFIX, "GPL3");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYURL_SUFFIX, "url2");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYLICENSEPATH_SUFFIX, "path of license");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYCOPYRIGHTOWNER_SUFFIX + 1, "owner1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 2 + Constant.DEPENDLIBRARYCOPYRIGHTOWNER_SUFFIX + 2, "owner2");
		return p;
	}

	@Test
	@DisplayName("Create TreeNode by Properties(no essential key)")
	void testCreateDependsLibraryFromProperties4() {
		assertNotNull(Util.createDependsLibraryFromProperties(createTestProperty9()));
	}
	
	Properties createTestProperty9() {
		Properties p = new Properties();
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYNAME_SUFFIX, "lib1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYVERSION_SUFFIX, "ver1");
		p.setProperty(Constant.DEPENDLIBRARY_PREFIX + 1 + Constant.DEPENDLIBRARYURL_SUFFIX, "url1");
		return p;
	}

	@Test
	@DisplayName("Create hyperlinked text")
	void testCreateHyperLink() {
		assertEquals("<html><a href=\"s\">s</a></html>", Util.createHyperlink("s"));
	}
}
