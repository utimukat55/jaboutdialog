package com.gitlab.utimukat55.jaboutdialog;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.gitlab.utimukat55.jaboutdialog.inner.Constant;

class JAboutDialogTest {

	@Test
	@DisplayName("Normal")
	void testSetVisible1() {
		JAboutDialog d = new JAboutDialog();
		d.setInfo(createProperties1());
		assertDoesNotThrow(() -> d.setVisible(true));
	}
	
	Properties createProperties1() {
		Properties p = new Properties();
		p.setProperty(Constant.SOFTWARENAME, "softwarename");
		p.setProperty(Constant.SOFTWARELICENSE, "softwarelicense");
		p.setProperty(Constant.SOFTWARECOPYRIGHT, "softwarecopyright");
		p.setProperty(Constant.SOFTWAREVERSION, "softwareversion");
		p.setProperty(Constant.SOFTWAREURL, "softwareurl");
		p.setProperty(Constant.SOFTWAREICON, "/icon64.png");
		return p;
	}
	
	@Test
	@DisplayName("Empty info")
	void testSetVisible2() {
		JAboutDialog d = new JAboutDialog();
		d.setInfo(createProperties2());
		assertDoesNotThrow(() -> d.setVisible(true));
	}
	
	Properties createProperties2() {
		Properties p = new Properties();
		return p;
	}

	@Test
	@DisplayName("Info is null")
	void testSetInfo1() {
		JAboutDialog d = new JAboutDialog();
		assertThrows(IllegalArgumentException.class, () -> d.setInfo(null));
	}

}
