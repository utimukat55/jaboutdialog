![JAboutDialog](icon64.png)

# JAboutDialog

Support to create "About dialog" in Java. Extends `javax.swing.JDialog`.

## Binary

Please download executable jar file below.

https://kasu-kasu.ga/post/jaboutdialog-relased-as-cc0/

# Prerequisites

- Java8+
- Maven installed PC

# How to use

## Maven build

JAboutDialog has 2 pom files, to build library jar and to create sample app runnable jar. The former is `pom.xml`, the latter is `pom_sampleapp.xml` .

### Build sample app

Sample app has JFrame and set values to call JAboutDialog. To make sample app, run commands below.

```
$ git clone https://gitlab.com/utimukat55/jaboutdialog.git
$ cd jaboutdialog/jaboutdialog
$ mvn -f pom_sampleapp.xml package
```

These commands make `jaboutdialog-0.0.3-sample-app.jar` (executable jar file) in `target` directory. `jaboutdialog-0-0-3.jar` not to use.

### Run sample app

To try sample app, execute commands below.

```
$ java -jar target/jaboutdialog-0.0.3-sample-app.jar
```

![JAboutDialog Sample App](./samplemain1.png)

Please choose `Help` -> `About` at menu.

![Choose Help -> About](samplemain2.png)

`About this Software` dialog is `JAboutDialog`. ”Show System Property / Environment Variable" button makes other dialog.

![JAboutDialog](jaboutdialog.png)

![Environment dialog](environmentdialog.png)

### Build library jar

To build simple jar library of JAboutDialog, run commands below.

```
$ git clone https://gitlab.com/utimukat55/jaboutdialog.git
$ cd jaboutdialog/jaboutdialog
$ mvn package
```

# Handling informations

JAboutDialog displays these informations.

- Image logo (64x64, using`javax.swing.ImageIcon`, if not 64x64, JAboutDialog expands/shrink icon size)
- Software name
- Software version
- Software URL (click-able)
- Software license
- Software copyright
- Using libraries (using `javax.swing.JTree`)
- Contributors

## Notes

![About License](license.png)

"Using libraries" needs these informations. Most of license except CC0 or Public Domain Software and so on needs license file's copy and copyright owner show to end users when distribution. It is strongly recommended that you set these informations.

- Library name
- (Optional) URL
- Library version (because sometimes license changes with version up)
- License
- (Optional) path of license files
- (Optional, can specify plural info) Copyright Owners

"Contributors" needs these informations.

- Name
- (Optional) URL

## Coding

Call `JAboutDialog` like calling `JDialog` and set informations with `JAboutDialog#setInfo(Properties)`. In sample app, xml Properties loads.

```java
JAboutDialog dialog = new JAboutDialog(f, true);
Properties prop = new Properties();
try (
  InputStream is = getClass().getResourceAsStream("/sampleinfo.xml")
) {
  prop.loadFromXML(is);
  dialog.setInfo(prop);
  dialog.setLocationRelativeTo(c);
  dialog.setVisible(true);
} catch (IOException e) {
  e.printStackTrace();
}
```

First argument of JAboutDialog's constructor can specify null. JAboutDialog overrides all JDialog's constructors, you can use any JDialog constructor.

JAboutDialog#setInfo()'s argument is `java.util.Properties`. You can make any way to create Properties instance.

### Info(Properties) format

JAboutDialog reads these keys from Properties instance.

- SoftwareName
- SoftwareVersion
- SoftwareUrl
- SoftwareLicense
- SoftwareCopyright
- SoftwareIcon
- DependLibrary-n.name
- DependLibrary-n.version
- DependLibrary-n.license
- DependLibrary-n.url
- DependLibrary-n.licensepath
- DependLibrary-n.copyrightowner-n
- Contributor-n.name
- Contributor-n.url

DependLibrary and Contributor need number from 1 (e.g. DependLibrary1, DependLibrary2, .., DependLibraryn).  DependLibrary-n.copyrightowner need number from 1 too, it can specify multi owner info. Example in sample app is below. 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
	<comment>Sample software info</comment>
	<entry key="SoftwareName">JAboutDialog</entry>
	<entry key="SoftwareVersion">0.0.1</entry>
	<entry key="SoftwareUrl">https://gitlab.com/utimukat55/jaboutdialog</entry>
	<!-- Common Licenses defined on com.gitlab.utimukat55.jaboutdialog.inner.CommonLicense.java 
		You can choose if described on it. -->
	<entry key="SoftwareLicense">CC0_1</entry>
	<entry key="SoftwareCopyright">This software and codes are licensed under CC0. https://creativecommons.org/publicdomain/zero/1.0/</entry>
 	<entry key="SoftwareIcon">/icon64.png</entry>
	<!-- DependLibrary n is read and will be listed (name, version and license are mandatory).-->
	<entry key="DependLibrary1.name">JTextComponent-Popup(example)</entry>
	<entry key="DependLibrary1.version">0.0.1</entry>
	<entry key="DependLibrary1.license">CC0_1</entry>
	<entry key="DependLibrary1.url">https://gitlab.com/utimukat55/jtextcomponent-popup</entry>

	<entry key="DependLibrary2.name">Maven Enforcer Plugin (only making jar)</entry>
	<entry key="DependLibrary2.version">3.0.0-M3</entry>
	<entry key="DependLibrary2.license">AL2</entry>
	<entry key="DependLibrary2.url">https://maven.apache.org/enforcer/maven-enforcer-plugin/</entry>
	<entry key="DependLibrary2.licensepath">license_maven_enforcer/ (in this sample, not include)</entry>
	<entry key="DependLibrary2.copyrightowner1">Copyright 2007-2013 The Apache Software Foundation</entry>

	<entry key="DependLibrary3.name">Apache Maven Compiler Plugin (only making jar)</entry>
	<entry key="DependLibrary3.version">3.8.1</entry>
	<entry key="DependLibrary3.license">AL2</entry>
	<entry key="DependLibrary3.url">https://maven.apache.org/plugins/maven-compiler-plugin/</entry>
	<entry key="DependLibrary3.licensepath">license_maven_compiler/ (in this sample, not include)</entry>
	<entry key="DependLibrary3.copyrightowner1">Copyright 2001-2019 The Apache Software Foundation</entry>

	<entry key="DependLibrary4.name">some library(example)</entry>
	<entry key="DependLibrary4.version">1.2.3</entry>
	<entry key="DependLibrary4.license">PROPRIETARY</entry>
	<entry key="DependLibrary4.licensepath">license_some_library/</entry>
	<entry key="DependLibrary4.copyrightowner1">Copyright (C) 2015 Bob</entry>
	<entry key="DependLibrary4.copyrightowner2">Copyright (C) 2021 Fictional works Inc.</entry>

	<!-- Contributor n is read and will be listed (name is mandatory). -->
	<entry key="Contributor1.name">utimukat55</entry>
	<entry key="Contributor1.url">http://gitlab.com/utimukat55</entry>
	
	<entry key="Contributor2.name">Alice</entry>
	<entry key="Contributor2.url">http://example.com/profile/alice</entry>
	
	<entry key="Contributor3.name">Bob</entry>
	<entry key="Contributor3.url">http://example.com/profile/bob</entry>
	
	<entry key="Contributor4.name">Charlie</entry>
	<entry key="Contributor5.name">Carol</entry>
	<entry key="Contributor6.name">Dave</entry>
</properties>
```

`SoftwareLicense` and `DependLibrary-n.license` can use abbreviation format defined in `inner/CommonLicense.java`. When specify "LGPL3PLUS", JAboutDialog replaces as "GNU LESSER GENERAL PUBLIC LICENSE Version 3 or any later version".

# Import to your project

## Import as jar

If your Java project is JSR376 moduled or not, you can import as module jar and as old-school library jar. Please use `jaboutdialog-0.0.2.jar` in target folder.

JAboutDialog exports module "com.gitlab.utimukat55.jaboutdialog" when import as JSR376 module.

## Import as java source files

JAboutDialog is licensed by CC0, you can use many ways. If you want to use as Java source file, please copy files below. (in `com.gitlab.utimukat55.jaboutdialog` package)

- inner/CommonLicense.java
- inner/Constant.java
- inner/LogUtil.java
- inner/Util.java
- EnvironmentDialog.java
- JAboutDialog.java

`usage` package contains sample app classes, `src/main/resources` directory contains sample Properties XML and icons, there are no need without running sample app.

# Improve this project

If you can't find license in `inner/CommonLicense.java`, Please tell the license and url.

# License

JAboutDialog is licensed by CC0 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed)).



# Other Information (written in Japanese)

https://kasu-kasu.ga/tags/jaboutdialog/
